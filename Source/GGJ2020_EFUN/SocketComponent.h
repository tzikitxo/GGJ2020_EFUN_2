// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "SocketComponent.generated.h"


UCLASS(Blueprintable)
class GGJ2020_EFUN_API USocketComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USocketComponent();

	UFUNCTION(BlueprintCallable)
	bool IsConnected() { return m_connection != nullptr; }

	UFUNCTION(BlueprintCallable)
	AActor * GetConnection() { return m_connection; }

	UFUNCTION(BlueprintCallable)
	void SetConnection(AActor * connection) 
	{
		m_connection = connection;
	}

	// use GetOwner to get this sockets owner

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// One connection per socket
	// connection needs to be actor because we also want to use player as connection
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor * m_connection = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_canBeMagnetized = true;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
