// Fill out your copyright notice in the Description page of Project Settings.


#include "Part.h"

// Sets default values
APart::APart()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void APart::DoAction_Implementation()
{
	// This function will be first called in blueprints 
	// Then this iwll be called throuhg "Richtclick add call to parent"

}

void APart::MagnitizeOpenSockets_Implementation(bool active)
{
	// This function will be first called in blueprints 
	// Then this will be called throuhg "Richtclick add call to parent"

}

void APart::ConnectToSocket_Implementation(USocketComponent * socket, USocketComponent* mySocket)
{
	if (socket->GetOwner() != this && mySocket != socket)
	{
		// we are not connecting to self
		m_parent = socket->GetOwner();
		mySocket->SetConnection(m_parent);
		socket->SetConnection(this);
		// Event to blueprints
		ConnectedToSocketEvent();
	}
}

void APart::ConnectedToSocketEvent_Implementation()
{
}

AActor * APart::GetConnectedParent_Implementation()
{
	return m_parent;
}

// Called when the game starts or when spawned
void APart::BeginPlay()
{
	Super::BeginPlay();
	// Get all sockets from this part
	GetComponents<USocketComponent>(m_sockets);
}

// Called every frame
void APart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

