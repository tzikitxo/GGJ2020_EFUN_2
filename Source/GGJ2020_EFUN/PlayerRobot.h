// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SocketComponent.h"
#include "PlayerRobot.generated.h"

UCLASS()
class GGJ2020_EFUN_API APlayerRobot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerRobot();

	UFUNCTION(BlueprintCallable)
	TArray<USocketComponent*> GetAllSockets()
	{
		return m_sockets;
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<USocketComponent*> m_sockets;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
