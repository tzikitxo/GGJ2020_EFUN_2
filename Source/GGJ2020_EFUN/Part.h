// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SocketComponent.h"
#include "Part.generated.h"

UCLASS()
class GGJ2020_EFUN_API APart : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APart();

	UFUNCTION(BlueprintCallable)
	TArray<USocketComponent*> GetAllSockets() 
	{
		return m_sockets;
	}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void DoAction();
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void MagnitizeOpenSockets(bool active);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ConnectToSocket(USocketComponent* socket, USocketComponent* mySocket);
	
	// Event
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ConnectedToSocketEvent();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	AActor * GetConnectedParent();

	UFUNCTION(BlueprintCallable)
	const bool GetIsPartOfRobot() const
	{
		return m_isAPartOfRobot;
	}

	UFUNCTION(BlueprintCallable)
	void SetIsPartOfRobot(bool value)
	{
		m_isAPartOfRobot = value;
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)	
	AActor * m_parent = nullptr;				

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_isAPartOfRobot = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<USocketComponent*> m_sockets;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
